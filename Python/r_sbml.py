#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2021 Clémence Frioux & Arnaud Belcour - Inria Dyliss - Pleiade
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>

from itertools import product
#from logging.config import _RootLoggerConfiguration
import re
import sys
import os
import xml.etree.ElementTree as etree
from xml.etree.ElementTree import XML, fromstring, tostring
from clyngor.as_pyasp import TermSet, Atom

def get_model(sbml):
    """Get the model of a SBML

    Args:
        sbml (str): SBML file

    Returns:
        xml.etree.ElementTree.Element: SBML model
    """
    model_element = None
    for e in sbml:
        if e.tag[0] == "{":
            uri, tag = e.tag[1:].split("}")
        else:
            tag = e.tag
        if tag == "model":
            model_element = e
            break
    return model_element

def get_listOfCompartments(model):
    """Get list of compartments in a SBML model
    
    Args:
        model (xml.etree.ElementTree.Element): SBML model
    
    Returns:
        xml.etree.ElementTree.Element: list of compartments
    """
    listOfCompartments = None
    for e in model:
        if e.tag[0] == "{":
          uri, tag = e.tag[1:].split("}")
        else: tag = e.tag
        if tag == "listOfCompartments":
          listOfCompartments = e
          break
    return listOfCompartments

def get_listOfSpecies(model):
    """Get list of Species of a SBML model
    
    Args:
        model (xml.etree.ElementTree.Element): SBML model
    
    Returns:
        xml.etree.ElementTree.Element: list of species
    """
    listOfSpecies = None
    for e in model:
        if e.tag[0] == "{":
            uri, tag = e.tag[1:].split("}")
        else:
            tag = e.tag
        if tag == "listOfSpecies":
            listOfSpecies = e
            break
    return listOfSpecies

def get_listOfReactions(model):
    """Get list of reactions of a SBML model
    
    Args:
        model (xml.etree.ElementTree.Element): SBML model
    
    Returns:
        xml.etree.ElementTree.Element: list of reactions
    """
    listOfReactions = None
    for e in model:
        if e.tag[0] == "{":
            uri, tag = e.tag[1:].split("}")
        else:
            tag = e.tag
        if tag == "listOfReactions":
            listOfReactions = e
            break
    return listOfReactions

def get_listOfReactants(reaction):
    """Get list of reactants of a reaction
    
    Args:
        reaction (xml.etree.ElementTree.Element): reaction
    
    Returns:
        xml.etree.ElementTree.Element: list of reactants
    """
    listOfReactants = None
    for e in reaction:
        if e.tag[0] == "{":
            uri, tag = e.tag[1:].split("}")
        else:
            tag = e.tag
        if tag == "listOfReactants":
            listOfReactants = e
            break
    return listOfReactants

def get_listOfProducts(reaction):
    """Get list of porducts of a reaction
    
    Args:
        reaction (xml.etree.ElementTree.Element): reaction
    
    Returns:
        xml.etree.ElementTree.Element: list of products
    """
    listOfProducts = None
    for e in reaction:
        if e.tag[0] == "{":
            uri, tag = e.tag[1:].split("}")
        else:
            tag = e.tag
        if tag == "listOfProducts":
            listOfProducts = e
            break
    return listOfProducts


def readSBLMnetworkCompartiment(filename):
    all_reaction = []
    all_reactant = []
    all_product = []
    tree = etree.parse(filename)
    sbml = tree.getroot()
    model = get_model(sbml)
    specie = model.attrib.get("id")
    listOfReactions = get_listOfReactions(model)
    for e in listOfReactions : 
        reaction_str = ""
        reactant_str = ""
        product_str = ""
        if e.tag[0] == "{":
            uri, tag = e.tag[1:].split("}")
        else:
            tag = e.tag
        if tag == "reaction":
            
            if e.attrib.get("reversible")=="true" :
                reactionId = e.attrib.get("id")
                reaction_str = 'reaction(' + '"' + reactionId+ '_rev",' + '"'+ specie + '").'
                all_reaction.append(reaction_str)
                reaction_str = 'reaction(' + '"' + reactionId + '",' + '"'+ specie + '").'
                all_reaction.append(reaction_str)
    
            else : 
                reactionId = e.attrib.get("id")
                reaction_str = 'reaction(' + '"' + reactionId + '",' + '"'+ specie + '").'
                all_reaction.append(reaction_str)

            listOfReactants = get_listOfReactants(e)
            listOfProducts = get_listOfProducts(e)
            if listOfReactants != None and listOfProducts != None:
                if e.attrib.get("reversible")=="true" :
                    for r in listOfProducts:
                        if (r.attrib.get("species")[-1] == "e") : 
                            reactant_str += 'reactant(' + '"' + r.attrib.get("species")[:-2] + '",' + '"' + reactionId + '_rev",' + '"'+ specie + '",' + '"e").'
                        else : #if (r.attrib.get("species")[-1] == "c") : 
                            reactant_str += 'reactant(' + '"' + r.attrib.get("species")[:-2] + '",' + '"' + reactionId + '_rev",' + '"'+ specie + '",' + '"c").'
                    #all_reactant.append(reactant_str)
                    for p in listOfReactants:
                        if (p.attrib.get("species")[-1] == "e") :
                            product_str += 'product(' + '"' + p.attrib.get("species")[:-2] + '",' + '"' + reactionId + '_rev",' + '"'+ specie + '",' + '"e").'
                        else : #if (p.attrib.get("species")[-1] == "c") :
                            product_str += 'product(' + '"' + p.attrib.get("species")[:-2] + '",' + '"' + reactionId + '_rev",' + '"'+ specie + '",' + '"c").'
                    #all_product.append(product_str)
                
            
                for r in listOfReactants:
                    if (r.attrib.get("species")[-1] == "e") : 
                        reactant_str += 'reactant(' + '"' + r.attrib.get("species")[:-2] + '",' + '"' + reactionId + '",' + '"'+ specie + '",' + '"e").'
                    else : #if (r.attrib.get("species")[-1] == "c") : 
                        reactant_str += 'reactant(' + '"' + r.attrib.get("species")[:-2] + '",' + '"' + reactionId + '",' + '"'+ specie + '",' + '"c").'
                all_reactant.append(reactant_str)
                for p in listOfProducts:  
                    if (p.attrib.get("species")[-1] == "e") :
                        product_str += 'product(' + '"' + p.attrib.get("species")[:-2] + '",' + '"' + reactionId + '",' + '"'+ specie + '",' + '"e").'
                    else : #if (p.attrib.get("species")[-1] == "c") :
                        product_str += 'product(' + '"' + p.attrib.get("species")[:-2] + '",' + '"' + reactionId + '",' + '"'+ specie + '",' + '"c").'
                all_product.append(product_str)
            else:
                print("reaction " + reactionId + " was ignored due to absent reactants or products")
            
            
    return all_reaction,all_reactant,all_product

def readSBLMnetworkCompartimentUniqueName(filename):
    all_reaction = []
    all_reactant = []
    all_product = []
    tree = etree.parse(filename)
    sbml = tree.getroot()
    model = get_model(sbml)
    specie = model.attrib.get("id")
    listOfReactions = get_listOfReactions(model)
    for e in listOfReactions : 
        reaction_str = ""
        reactant_str = ""
        product_str = ""
        if e.tag[0] == "{":
            uri, tag = e.tag[1:].split("}")
        else:
            tag = e.tag
        if tag == "reaction":
            
            if e.attrib.get("reversible")=="true" :
                reactionId = e.attrib.get("id")
                reaction_str = 'reaction(' + '"' + reactionId+ '_rev",' + '"'+ specie + '").'
                all_reaction.append(reaction_str)
                reaction_str = 'reaction(' + '"' + reactionId + '",' + '"'+ specie + '").'
                all_reaction.append(reaction_str)
    
            else : 
                reactionId = e.attrib.get("id")
                reaction_str = 'reaction(' + '"' + reactionId + '",' + '"'+ specie + '").'
                all_reaction.append(reaction_str)

            listOfReactants = get_listOfReactants(e)
            listOfProducts = get_listOfProducts(e)
            if listOfReactants != None and listOfProducts != None:
                if e.attrib.get("reversible")=="true" :
                    for r in listOfProducts:
                        if (r.attrib.get("species")[-1] == "e") : 
                            reactant_str += 'reactant(' + '"' + r.attrib.get("species") + '",' + '"' + reactionId + '_rev",' + '"'+ specie + '",' + '"e").'
                        else : #if (r.attrib.get("species")[-1] == "c") : 
                            reactant_str += 'reactant(' + '"' + r.attrib.get("species") + '",' + '"' + reactionId + '_rev",' + '"'+ specie + '",' + '"c").'
                    #all_reactant.append(reactant_str)
                    for p in listOfReactants:
                        if (p.attrib.get("species")[-1] == "e") :
                            product_str += 'product(' + '"' + p.attrib.get("species") + '",' + '"' + reactionId + '_rev",' + '"'+ specie + '",' + '"e").'
                        else : #if (p.attrib.get("species")[-1] == "c") :
                            product_str += 'product(' + '"' + p.attrib.get("species") + '",' + '"' + reactionId + '_rev",' + '"'+ specie + '",' + '"c").'
                    #all_product.append(product_str)
                
            
                for r in listOfReactants:
                    if (r.attrib.get("species")[-1] == "e") : 
                        reactant_str += 'reactant(' + '"' + r.attrib.get("species") + '",' + '"' + reactionId + '",' + '"'+ specie + '",' + '"e").'
                    else : #if (r.attrib.get("species")[-1] == "c") : 
                        reactant_str += 'reactant(' + '"' + r.attrib.get("species") + '",' + '"' + reactionId + '",' + '"'+ specie + '",' + '"c").'
                all_reactant.append(reactant_str)
                for p in listOfProducts:  
                    if (p.attrib.get("species")[-1] == "e") :
                        product_str += 'product(' + '"' + p.attrib.get("species") + '",' + '"' + reactionId + '",' + '"'+ specie + '",' + '"e").'
                    else : #if (p.attrib.get("species")[-1] == "c") :
                        product_str += 'product(' + '"' + p.attrib.get("species") + '",' + '"' + reactionId + '",' + '"'+ specie + '",' + '"c").'
                all_product.append(product_str)
            else:
                print("reaction " + reactionId + " was ignored due to absent reactants or products")
            
            
    return all_reaction,all_reactant,all_product

def readSBLMnetwork(filename):
    all_reaction = []
    all_reactant = []
    all_product = []
    tree = etree.parse(filename)
    sbml = tree.getroot()
    model = get_model(sbml)
    specie = model.attrib.get("id")
    listOfReactions = get_listOfReactions(model)
    for e in listOfReactions : 
        reaction_str = ""
        reactant_str = ""
        product_str = ""
        if e.tag[0] == "{":
            uri, tag = e.tag[1:].split("}")
        else:
            tag = e.tag
        if tag == "reaction":
            
            if e.attrib.get("reversible")=="true" :
                reactionId = e.attrib.get("id")
                reaction_str = 'reaction(' + '"' + reactionId+ '_rev",' + '"'+ specie + '").'
                all_reaction.append(reaction_str)
                reaction_str = 'reaction(' + '"' + reactionId + '",' + '"'+ specie + '").'
                all_reaction.append(reaction_str)
    
            else : 
                reactionId = e.attrib.get("id")
                reaction_str = 'reaction(' + '"' + reactionId + '",' + '"'+ specie + '").'
                all_reaction.append(reaction_str)

            listOfReactants = get_listOfReactants(e)
            listOfProducts = get_listOfProducts(e)
            if listOfReactants != None and listOfProducts != None:
                if e.attrib.get("reversible")=="true" :
                    for r in listOfProducts:
                        if (r.attrib.get("species")[-1] == "e") : 
                            reactant_str += 'reactant(' + '"' + r.attrib.get("species") + '",' + '"' + reactionId + '_rev",' + '"'+ specie + '").'
                        else : #if (r.attrib.get("species")[-1] == "c") : 
                            reactant_str += 'reactant(' + '"' + r.attrib.get("species")+ '",' + '"' + reactionId + '_rev",' + '"'+ specie + '").'
                    #all_reactant.append(reactant_str)
                    for p in listOfReactants:
                        if (p.attrib.get("species")[-1] == "e") :
                            product_str += 'product(' + '"' + p.attrib.get("species") + '",' + '"' + reactionId + '_rev",' + '"'+ specie + '").'
                        else : #if (p.attrib.get("species")[-1] == "c") :
                            product_str += 'product(' + '"' + p.attrib.get("species")+ '",' + '"' + reactionId + '_rev",' + '"'+ specie + '").'
                    #all_product.append(product_str)
                
            
                for r in listOfReactants:
                    if (r.attrib.get("species")[-1] == "e") : 
                        reactant_str += 'reactant(' + '"' + r.attrib.get("species") + '",' + '"' + reactionId + '",' + '"'+ specie + '").'
                    else : #if (r.attrib.get("species")[-1] == "c") : 
                        reactant_str += 'reactant(' + '"' + r.attrib.get("species")+ '",' + '"' + reactionId + '",' + '"'+ specie + '").'
                all_reactant.append(reactant_str)
                for p in listOfProducts:  
                    if (p.attrib.get("species")[-1] == "e") :
                        product_str += 'product(' + '"' + p.attrib.get("species") + '",' + '"' + reactionId + '",' + '"'+ specie + '").'
                    else : #if (p.attrib.get("species")[-1] == "c") :
                        product_str += 'product(' + '"' + p.attrib.get("species")+ '",' + '"' + reactionId + '",' + '"'+ specie + '").'
                all_product.append(product_str)
            else:
                print("reaction " + reactionId + " was ignored due to absent reactants or products")
            
            
    return all_reaction,all_reactant,all_product

def readXMLseed(filename):
    seed = []
    tree = etree.parse(filename)
    sbml = tree.getroot()
    model = get_model(sbml)
    listOfSpecies = get_listOfSpecies(model)
    for e in listOfSpecies : 
        if e.tag[0] == "{":
            uri, tag = e.tag[1:].split("}")
        else:
            tag = e.tag
        if tag == "species":
            compoundID = e.attrib.get("id")[:-1]
            compoundID += "e"
            seed.append("seed(" + '"' + compoundID[:-2] + '").') 

    return seed



def write_facts(all_facts,filename):
    specie = filename.split(".")
    new_file = "/Users/cldd/Stage_Quentin/ltl_stagequentin/instances_"+specie[0]+".lp"
    f = open(new_file, "w")
    for i in range(len(all_facts)):
        if (i ==0):
            f.write("#program always."+"\n")
        for j in range(len(all_facts[i])):
            f.write(all_facts[i][j] + "\n")
    f.close()

def write_seed(tab) : 
    file = "/Users/cldd/Stage_Quentin/ltl_stagequentin/seeds.lp"
    f = open(file,"w")
    for i in range(len(tab)):
        if (i ==0):
            f.write("#program always."+"\n")
        else : 
            f.write(tab[i] + "\n")


path = os. getcwd()
files = os.listdir(path +"/Networks")
for filename in files:
    if filename.endswith(".xml") : 
        seeds = readXMLseed(path +"/Networks/"+filename)
        write_seed(seeds)
    else : 
        path_file = path +"/Networks/"+filename
        all_facts = readSBLMnetworkCompartimentUniqueName(path_file)
        write_facts(all_facts,filename)
        
    


