
import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-p",
                    "--prog",
                    action = "store",
                    choices=["explication" , "simulation","planning","verification"],
                    required=True)
args = parser.parse_args()
print(args)


import parse_result_exp as p


#main
if (args.prog == "explication") :
    facts = p.read_data('/Users/cldd/Stage_Quentin/result_dfba_com.txt')
    p.write_instance(facts)
    p1 = subprocess.run(['telingo' , '/Users/cldd/Stage_Quentin/ltl_stagequentin/instances.lp' , '/Users/cldd/Stage_Quentin/ltl_stagequentin/test_ltl_explication.lp'])
    print(p1.stdout)
if (args.prog == "simulation") :
    p1 = subprocess.run(['telingo' , '/Users/cldd/Stage_Quentin/ltl_stagequentin/test_ltl_simulation.lp'])
    print(p1.stdout)
if (args.prog == "planning") :
    p1 = subprocess.run(['telingo' , '/Users/cldd/Stage_Quentin/ltl_stagequentin/test_ltl_planning.lp'])
    print(p1.stdout)
if (args.prog == "verification") :
    facts = p.read_data('/Users/cldd/Stage_Quentin/result_dfba_com.txt')
    p.write_instance(facts)
    p1 = subprocess.run(['telingo' , '/Users/cldd/Stage_Quentin/ltl_stagequentin/instances.lp' , '/Users/cldd/Stage_Quentin/ltl_stagequentin/test_ltl_verification.lp'])
    print(p1.stdout)