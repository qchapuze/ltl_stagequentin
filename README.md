# Stage Quentin


## Description

The tool developed during my internship allows me to answer specific biological questions.
It is possible to answer 3 cases of application: 
- planning : selection of a minimal community to produce a target compound
- explanation : explanation of the presence of a compound knowing that a compound has been observed and is necessary
- verification : Verify the producibility of one or several target compounds 


## Usage 

**Telingo**

First, create a conda environment and use this command to get Telingo : 

```
conda install -c conda-forge telingo
```

**On the didactic data**

In the case of planning : 

```
telingo toy1.lp planning.lp 0
```
In the case of explication : 

```
telingo toy1.lp explication.lp 0
```

**On the real data** 

In the case of planning :

```
telingo ../Instances/instances_plantarum_U.lp ../Instances/instances_freudenreichii2_U.lp ../Instances/instances_lactis_U.lp ../Instances/seeds_woc.lp planning.lp  0
```

In the case of explication : 

```
telingo ../Instances/instances_plantarum_U.lp ../Instances/instances_freudenreichii2_U.lp ../Instances/instances_lactis_U.lp ../Instances/seeds_woc.lp explication.lp  0
```

In the case of verification : 

```
telingo ../Instances/instances_plantarum_U.lp ../Instances/instances_freudenreichii2_U.lp ../Instances/instances_lactis_U.lp ../Instances/seeds_woc.lp verification.lp --time-limit=<n> 0
```

**Transforming networks into an ASP fact base**

Put your networks in the Network directory 

and : 

```
python3 r_sbml.py
```
